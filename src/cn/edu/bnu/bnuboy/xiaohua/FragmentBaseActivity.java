package cn.edu.bnu.bnuboy.xiaohua;


import cn.edu.bnu.bnuboy.fragments.MenuListFragment;
import cn.edu.bnu.bnuboy.fragments.ReciteList;

import com.baidu.mobads.InterstitialAd;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * 应用程序Activity的基�?
 * 
 * @author liux (http://my.oschina.net/liux)
 * @version 1.0
 * @created 2012-9-18
 */
public abstract class FragmentBaseActivity extends FragmentActivity {
	public Context ctx;
	private Fragment mContent;
	public SlidingMenu menu;
	// 是否允许全屏
	private boolean allowFullScreen = true;

	// 是否允许�?��
	private boolean allowDestroy = true;

	private View view;
	private InterstitialAd interAd;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		interAd=new InterstitialAd(this);
		if(interAd.isAdReady()){
			interAd.showAd(this);
		}else{
			interAd.loadAd();
		}
		allowFullScreen = true;
		ctx = this;
		// 添加Activity到堆�?
		main();
		menu = new SlidingMenu(this);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
//		menu.setShadowWidthRes(R.dimen.shadow_width);
//		menu.setShadowDrawable(R.drawable.shadow);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
//		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setMenu(R.layout.menu_frame);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.menu_frame, new MenuListFragment())
		.commit();
	}
	abstract public void main();
	@Override
	public void onBackPressed() {
		if (menu.isMenuShowing()) {
			menu.showContent();
		} else {
			super.onBackPressed();
		}
	}
	
	public void showMenu(View v){
		menu.showMenu();
	}
	
	public void switchContent(final Fragment fragment) {
		mContent = fragment;
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.main, fragment)
		.addToBackStack(null)
		.commit();
		Handler h = new Handler();
		h.postDelayed(new Runnable() {
			public void run() {
				menu.showContent();
			}
		}, 50);
		if(interAd.isAdReady()){
			interAd.showAd(this);
		}else{
			interAd.loadAd();
		}
	}
	protected Dialog onCreateDialog(int id) {
		ProgressDialog dialog = new ProgressDialog(this);
		dialog.setMessage("正在打开...");
		dialog.setIndeterminate(true);
		dialog.setCancelable(false);
		return dialog;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		// 结束Activity&从堆栈中移除
		finish();
	}

	public boolean isAllowFullScreen() {
		return allowFullScreen;
	}

	/**
	 * 设置是否可以全屏
	 * 
	 * @param allowFullScreen
	 */
	public void setAllowFullScreen(boolean allowFullScreen) {
		this.allowFullScreen = allowFullScreen;
	}

	public void setAllowDestroy(boolean allowDestroy) {
		this.allowDestroy = allowDestroy;
	}

	public void setAllowDestroy(boolean allowDestroy, View view) {
		this.allowDestroy = allowDestroy;
		this.view = view;
	}

	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
        	if(getSupportFragmentManager().getBackStackEntryCount()>1){
	            getSupportFragmentManager().popBackStack();;
	            return true;
        	}else{
        		if (keyCode == KeyEvent.KEYCODE_BACK )
                {
                    if ((System.currentTimeMillis() - exitTime) > 2000)
                    {
                        Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
                        exitTime = System.currentTimeMillis();
                    }
                    else
                    {
                              finish();
                        System.exit(0);
                    }
                    return true;
                }
        	}
        }
        return super.onKeyDown(keyCode, event);
    }
    
    private long exitTime = 0;
}
