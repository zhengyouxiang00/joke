package cn.edu.bnu.bnuboy.xiaohua;

import net.youmi.android.spot.SpotManager;

import com.baidu.mobads.InterstitialAd;

import cn.edu.bnu.bnuboy.utils.DBUtils;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public class WelcomeActivity extends Activity {
	private SharedPreferences pref;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = View.inflate(this, R.layout.welcom, null);
        setContentView(view);
        pref = getSharedPreferences(getText(R.string.preference_name).toString(),MODE_PRIVATE);
        if(!pref.getBoolean("is_first_run", false)){
			DBUtils dbUtils =new DBUtils(this);
			dbUtils.copy(R.raw.xutils, "xUtils.db");
			Editor editor = pref.edit();
			editor.putBoolean("is_first_run", true);
			editor.commit();
		}
        AlphaAnimation aa = new AlphaAnimation(0.5f,1.0f);
		aa.setDuration(3000);
		view.startAnimation(aa);
		aa.setAnimationListener(new AnimationListener()
		{
			public void onAnimationEnd(Animation arg0) {
				Intent intent = new Intent(WelcomeActivity.this, IndexActivity.class);
		        startActivity(intent);
                finish();
			}
			public void onAnimationRepeat(Animation animation) {}
			public void onAnimationStart(Animation animation) {}
			
		});
        
        
    }
}
