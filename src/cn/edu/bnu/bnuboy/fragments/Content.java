package cn.edu.bnu.bnuboy.fragments;

import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.exception.DbException;

import cn.edu.bnu.bnuboy.db.Xiaohua;
import cn.edu.bnu.bnuboy.xiaohua.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Content extends Fragment {
	private TextView cn;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.words_list, null);
		cn = (TextView)v.findViewById(R.id.cn);
		DbUtils db = DbUtils.create(this.getActivity());
		int id = this.getArguments().getInt("id");
		try {
			Xiaohua x = db.findById(Xiaohua.class, id);
			cn.setText(Html.fromHtml(x.content));
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return v;
	}
}
