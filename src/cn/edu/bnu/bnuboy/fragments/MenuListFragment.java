package cn.edu.bnu.bnuboy.fragments;



import cn.edu.bnu.bnuboy.xiaohua.FragmentBaseActivity;
import cn.edu.bnu.bnuboy.xiaohua.R;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MenuListFragment extends ListFragment {

	private String[] args = new String[]{"校园笑话","家庭笑话","儿童笑话","现代笑话","交通笑话","体育笑话"
			,"军事笑话","冷笑话","医疗笑话","古代笑话","名人笑话","国外笑话","愚人笑话"
			,"民间笑话","爱情笑话","电脑笑话","经营笑话","职场笑话"};
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.menu_list, null);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		SampleAdapter adapter = new SampleAdapter(getActivity());
		adapter.add(new SampleItem("校园笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("家庭笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("儿童笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("现代笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("交通笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("体育笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("军事笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("冷笑话",   android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("医疗笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("古代笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("名人笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("国外笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("愚人笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("民间笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("爱情笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("电脑笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("经营笑话", android.R.drawable.ic_menu_info_details));
		adapter.add(new SampleItem("职场笑话", android.R.drawable.ic_menu_info_details));
		setListAdapter(adapter);
	}
	

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		Fragment f = new ReciteList();
		Bundle bundle = new Bundle();
		bundle.putString("type", args[position]);
		f.setArguments(bundle);
		switchFragment(f);
	}
	private void switchFragment(Fragment fragment) {
		if (getActivity() == null)
			return;
		((FragmentBaseActivity) getActivity()).switchContent(fragment);
	}


	private class SampleItem {
		public String tag;
		public int iconRes;
		public SampleItem(String tag, int iconRes) {
			this.tag = tag; 
			this.iconRes = iconRes;
		}
	}

	public class SampleAdapter extends ArrayAdapter<SampleItem> {

		public SampleAdapter(Context context) {
			super(context, 0);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, null);
			}
			ImageView icon = (ImageView) convertView.findViewById(R.id.row_icon);
			icon.setImageResource(getItem(position).iconRes);
			TextView title = (TextView) convertView.findViewById(R.id.row_title);
			title.setText(getItem(position).tag);

			return convertView;
		}

	}
}
