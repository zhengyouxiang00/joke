package cn.edu.bnu.bnuboy.fragments;

import java.util.List;

import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.exception.DbException;

import cn.edu.bnu.bnuboy.db.Xiaohua;
import cn.edu.bnu.bnuboy.xiaohua.FragmentBaseActivity;
import cn.edu.bnu.bnuboy.xiaohua.R;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ReciteList extends Fragment {
	Cursor c;
	private ListView lv;
	private DbUtils db;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.test_list, null);
		DbUtils db = DbUtils.create(this.getActivity());
		Bundle bundle = this.getArguments();
		String type = "";
		if(null == bundle){
			type = "儿童笑话";
		}else{
			type = bundle.getString("type");
		}
		try {
			final List<Xiaohua> list = db.findAll(Selector.from(Xiaohua.class).where("type","=",type));
			lv = (ListView)v.findViewById(R.id.unitlist);
			String[] str = new String[list.size()];
			for(int i = 0;i < str.length; i ++){
				Xiaohua x = list.get(i);
				str[i] = x.title;
			}
			ArrayAdapter<String> ad = new ArrayAdapter<String>(this.getActivity().getApplicationContext(),R.layout.list_style, str);
			lv.setAdapter(ad);
			lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View arg1,int pos, long arg3) {
					Bundle bundle = new Bundle();
					bundle.putInt("id", list.get(pos).id);
					Fragment f = new Content();
					f.setArguments(bundle);
					((FragmentBaseActivity) getActivity()).switchContent(f);
					
				}
			});
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return v;
	}
	
}
