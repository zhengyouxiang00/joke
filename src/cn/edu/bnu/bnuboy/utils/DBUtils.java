package cn.edu.bnu.bnuboy.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import android.content.Context;


public class DBUtils {
//	private static final String DATABASE_PATH = "/data/data/cn.edu.bnu.bnuboy.xiaohua/databases";
	private Context ctx;
	public DBUtils(Context ctx){
		this.ctx = ctx;
	}
	public void copy(int res,String name){
		try{
			String packageName = ctx.getPackageName();
			String path = "/data/data/" + packageName + "/databases";
	        String databaseFilename = path + "/xUtils.db"; 
	    	File dir = new File(path); 
	    	 
	    	if (!dir.exists()) 
	    	    dir.mkdir(); 
	    	 
	    	if (!(new File(databaseFilename)).exists()) { 
	    	    InputStream is = ctx.getResources().openRawResource(res); 
	    	    FileOutputStream fos = new FileOutputStream(databaseFilename); 
	    	    byte[] buffer = new byte[8192]; 
	    	    int count = 0; 
	    	     
	    	    while ((count = is.read(buffer)) > 0) { 
	    	        fos.write(buffer, 0, count);  
	    	    } 
	    	 
	    	    fos.close(); 
	    	    is.close(); 
	    	}
        }catch(Exception e){
        	e.printStackTrace();
        }
	}
}
